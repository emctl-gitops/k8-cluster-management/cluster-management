---
# Source: kyverno/templates/deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: 30-kyverno-kyverno
  labels: 
    app.kubernetes.io/component: kyverno
    app.kubernetes.io/instance: 30-kyverno-kyverno
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/name: kyverno
    app.kubernetes.io/part-of: kyverno
    app.kubernetes.io/version: "v2.3.1"
    helm.sh/chart: kyverno-v2.3.1
    app: kyverno
  namespace: security
spec:
  selector:
    matchLabels: 
      app.kubernetes.io/name: kyverno
      app.kubernetes.io/instance: 30-kyverno-kyverno
  replicas: 1
  strategy:
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 40%
    type: RollingUpdate
  template:
    metadata:
      labels: 
        app.kubernetes.io/component: kyverno
        app.kubernetes.io/instance: 30-kyverno-kyverno
        app.kubernetes.io/managed-by: Helm
        app.kubernetes.io/name: kyverno
        app.kubernetes.io/part-of: kyverno
        app.kubernetes.io/version: "v2.3.1"
        helm.sh/chart: kyverno-v2.3.1
        app: kyverno
    spec:
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: app.kubernetes.io/name
                  operator: In
                  values:
                  - kyverno
              topologyKey: kubernetes.io/hostname
            weight: 1
      serviceAccountName: 30-kyverno-kyverno
      dnsPolicy: ClusterFirst
      initContainers:
        - name: kyverno-pre
          image: ghcr.io/kyverno/kyvernopre:v1.6.1
          imagePullPolicy: IfNotPresent
          resources: 
            limits:
              cpu: 100m
              memory: 256Mi
            requests:
              cpu: 10m
              memory: 64Mi
          securityContext:
            runAsNonRoot: true
            privileged: false
            allowPrivilegeEscalation: false
            readOnlyRootFilesystem: true
            capabilities:
              drop:
                - ALL
          env:
          - name: METRICS_CONFIG
            value: 30-kyverno-kyverno-metrics
          - name: KYVERNO_NAMESPACE
            valueFrom:
              fieldRef:
                fieldPath: metadata.namespace
          - name: KYVERNO_DEPLOYMENT
            value: 30-kyverno-kyverno
      containers:
        - name: kyverno
          image: ghcr.io/kyverno/kyverno:v1.6.1
          imagePullPolicy: IfNotPresent
          resources: 
            limits:
              memory: 384Mi
            requests:
              cpu: 100m
              memory: 128Mi
          securityContext:
            runAsNonRoot: true
            privileged: false
            allowPrivilegeEscalation: false
            readOnlyRootFilesystem: true
            capabilities:
              drop:
                - ALL
          ports:
          - containerPort: 9443
            name: https
            protocol: TCP
          - containerPort: 8000
            name: metrics-port
            protocol: TCP
          env:
          - name: INIT_CONFIG
            value: 30-kyverno-kyverno
          - name: METRICS_CONFIG
            value: 30-kyverno-kyverno-metrics
          - name: KYVERNO_NAMESPACE
            valueFrom:
              fieldRef:
                fieldPath: metadata.namespace
          - name: KYVERNO_SVC
            value: 30-kyverno-kyverno-svc
          - name: KYVERNO_DEPLOYMENT
            value: 30-kyverno-kyverno
          livenessProbe: 
            failureThreshold: 2
            httpGet:
              path: /health/liveness
              port: 9443
              scheme: HTTPS
            initialDelaySeconds: 15
            periodSeconds: 30
            successThreshold: 1
            timeoutSeconds: 5
          readinessProbe: 
            failureThreshold: 6
            httpGet:
              path: /health/readiness
              port: 9443
              scheme: HTTPS
            initialDelaySeconds: 5
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 5
